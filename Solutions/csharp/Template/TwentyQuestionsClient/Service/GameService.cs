﻿using System;
using TwentyQuestionsClient.Models;

namespace TwentyQuestionsClient.Service
{
    public class GameService : IGameService
    {
        public GameNameResponse Name()
        {
            throw new NotImplementedException();
        }

        GameResponse IGameService.StartGame(string gameId)
        {
            throw new NotImplementedException();
        }

        GameResponse IGameService.HandleAnswer(string gameId, int questionId, Models.GameResponse.Answer type)
        {
            throw new NotImplementedException();
        }

        public void WinGame(string gameId, int questionId)
        {
            throw new NotImplementedException();
        }

        public void LoseGame(string gameId, int questionId)
        {
            throw new NotImplementedException();
        }
    }
}