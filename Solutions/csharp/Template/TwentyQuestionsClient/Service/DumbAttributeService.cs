﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using TwentyQuestionsClient.Models;

namespace TwentyQuestionsClient.Service
{
    public class DumbAttributeService : IGameService
    {
        private static readonly ConcurrentDictionary<string, List<Animal>> Games = new ConcurrentDictionary<string, List<Animal>>();
        private const int AnimalGuess = -1;
        private const int GiveUp = -2;
        private const int Comment = -3;

        #region IGameService
        public GameNameResponse Name()
        {
            return new GameNameResponse("Dumb attribute-based game");
        }

        public GameResponse StartGame(string gameId)
        {
            Games.AddOrUpdate(gameId, CreateListOfAnimals(), (s, list) => list);
            return GenerateQuestion(gameId);
        }

        public GameResponse HandleAnswer(string gameId, int questionId, GameResponse.Answer answer)
        {
            if (questionId == AnimalGuess)
            {
                if (answer == GameResponse.Answer.Yes)
                {                    
                    return GenerateCommentResponse("Then shouldn't you say that I won?");
                }
                if (answer == GameResponse.Answer.No)
                {
                    var animals = Games[gameId];
                    animals.RemoveAt(0);
                    Games[gameId] = animals;
                }
                return GenerateQuestion(gameId);
            }

            if (questionId >= 0)
            {
                FilterRemainingAnimals(gameId, questionId, answer);
                return GenerateQuestion(gameId);
            }

            if (questionId == Comment)
            {
                return GenerateCommentResponse("You shouldn't respond to a comment with yes or no. Can you click Win or Lose?");
            }

            throw new Exception("Unexpected response");
        }

        public void WinGame(string gameId, int questionId)
        {
            EndGame(gameId);
        }

        public void LoseGame(string gameId, int questionId)
        {
            EndGame(gameId);
        }
        #endregion

        #region Helper functions
        private static void FilterRemainingAnimals(string gameId, int questionId, GameResponse.Answer answer)
        {
            var animals = Games[gameId];
            switch (answer)
            {
                case GameResponse.Answer.Yes:
                    animals =
                        animals.Where(animal => animal.Attributes.Contains((AnimalAttribute)questionId)).ToList();
                    break;
                case GameResponse.Answer.No:
                    animals = animals.Where(animal => !animal.Attributes.Contains((AnimalAttribute)questionId)).ToList();
                    break;
                case GameResponse.Answer.Skip:
                    // do nothing
                    break;
                default:
                    throw new KeyNotFoundException(string.Format("Unrecognized game answer {0}", answer));
            }
            Games[gameId] = animals;
        }

        private static GameResponse GenerateQuestion(string gameId)
        {
            var animalsRemaining = Games[gameId];

            return GenerateNextQuestion(animalsRemaining);
        }

        private static void EndGame(string gameId)
        {
            List<Animal> value;
            Games.TryRemove(gameId, out value);
        }
        #endregion

        #region Functions for creating specific types of responses

        private static GameResponse GenerateNextQuestion(List<Animal> animals)
        {
            var response = GenerateAttributeGameResponse(animals);

            return response ?? GenerateAnimalGameResponse(animals);
        }

        /// <summary>
        /// Look for a unique attribute among the animals. If we find one, we generate a question. Otherwise, we return null
        /// </summary>
        /// <param name="animals"></param>
        /// <returns></returns>
        private static GameResponse GenerateAttributeGameResponse(IReadOnlyList<Animal> animals)
        {
            GameResponse response = null;
            for (var i = 0; i < animals.Count; i++)
            {
                for (var j = i + 1; j < animals.Count; j++)
                {
                    var attributes1 = animals[i].Attributes;
                    var attributes2 = animals[j].Attributes;
                    var uniqueAttributes = attributes1.Except(attributes2).ToList();

                    if (!uniqueAttributes.Any()) continue;

                    var firstUniqueAttribute = uniqueAttributes.First();
                    response = new GameResponse
                    {
                        Id = (int) firstUniqueAttribute,
                        Question = LookupQuestion(firstUniqueAttribute),
                        Type = GameResponse.ResponseType.Question
                    };
                }
            }
            return response;
        }

        /// <summary>
        /// Generate a question based on the first animal in the remaining list
        /// </summary>
        /// <param name="animalsRemaining"></param>
        /// <returns></returns>
        private static GameResponse GenerateAnimalGameResponse(List<Animal> animalsRemaining)
        {
            if (animalsRemaining.Any())
            {
                var animal = animalsRemaining.First();

                return new GameResponse
                {
                    Id = AnimalGuess,
                    Question = "Is it a/an " + animal.Name + "?",
                    Type = GameResponse.ResponseType.Question
                };
            }
            return GenerateGiveUpResponse();
        }

        private static GameResponse GenerateGiveUpResponse()
        {
            return new GameResponse
            {
                Id = GiveUp,
                Question = "I've run out of possible answers",
                Type = GameResponse.ResponseType.GiveUp
            };
        }

        private static GameResponse GenerateCommentResponse(string response)
        {
            return new GameResponse
            {
                Id = Comment,
                Question = response,
                Type = GameResponse.ResponseType.Question
            };
        }


        #endregion

        #region Data about animals
        public enum AnimalAttribute
        {
            Mammal,
            FourLegs,
            Pet,
            Fly,
            LivesInWater
        }

        private static List<Animal> CreateListOfAnimals()
        {
            return new List<Animal>
            {
                new Animal{Name = "Dog", Attributes = new List<AnimalAttribute> { AnimalAttribute.Mammal, AnimalAttribute.FourLegs, AnimalAttribute.Pet }},
                new Animal{Name = "Lion", Attributes = new List<AnimalAttribute> { AnimalAttribute.Mammal, AnimalAttribute.FourLegs }},
                new Animal{Name = "Carp", Attributes = new List<AnimalAttribute> { AnimalAttribute.LivesInWater }},
                new Animal{Name = "Cat", Attributes = new List<AnimalAttribute> { AnimalAttribute.Mammal, AnimalAttribute.FourLegs, AnimalAttribute.Pet }}
            };
        }

        private static string LookupQuestion(AnimalAttribute attribute)
        {
            switch (attribute)
            {
                case AnimalAttribute.Fly:
                    return "Can it fly?";
                case AnimalAttribute.FourLegs:
                    return "Does it have four legs?";
                case AnimalAttribute.LivesInWater:
                    return "Does it live in the water?";
                case AnimalAttribute.Mammal:
                    return "Is it a mammal?";
                case AnimalAttribute.Pet:
                    return "Do people keep it as a pet?";
                default:
                    throw new KeyNotFoundException(string.Format("Unrecognized animal attribute {0}", attribute));
            }
        }
        #endregion
    }

    public class Animal
    {
        public string Name { get; set; }
        public List<DumbAttributeService.AnimalAttribute> Attributes { get; set; } 
    }
}