﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using TwentyQuestionsClient.Models;

namespace TwentyQuestionsClient.Service
{
    public class DumbGameService : IGameService
    {
        private static readonly ConcurrentDictionary<string, int> Games = new ConcurrentDictionary<string, int>();

        private readonly GameResponse[] m_responses =
        {             
            new GameResponse { Id = 1, Question = "Do you have hair?", Type = GameResponse.ResponseType.Question}, 
            new GameResponse { Id = 2, Question = "Can you fly?", Type = GameResponse.ResponseType.Question}, 
            new GameResponse { Id = 3, Question = "Are you a cat?", Type = GameResponse.ResponseType.Question}, 
            new GameResponse { Id = 4, Question = "Are you a dog?", Type = GameResponse.ResponseType.Question}, 
            new GameResponse { Id = 5, Question = "Are you a whale?", Type = GameResponse.ResponseType.Question}, 
            new GameResponse { Id = 6, Type = GameResponse.ResponseType.GiveUp}            
        };

        public GameNameResponse Name()
        {
            // Identifies the current solution so that we can keep track of its scores
            return new GameNameResponse("Dumb Game Service");
        }

        public GameResponse StartGame(string gameId)
        {
            // This dummy game service will cycle through the game responses from 1 through 6.
            Games.AddOrUpdate(gameId, 0, (s, i) => i);
            return GenerateQuestion(gameId);
        }

        public GameResponse HandleAnswer(string gameId, int questionId, GameResponse.Answer answer)
        {
            // Typically, a better game service would take the previous questionId and answer into account
            return GenerateQuestion(gameId);
        }

        public void WinGame(string gameId, int questionId)
        {
            EndGame(gameId);
        }

        public void LoseGame(string gameId, int questionId)
        {
            EndGame(gameId);
        }

        private GameResponse GenerateQuestion(string gameId)
        {
            try
            {
                var nextQuestionId = Games[gameId];
                var rand = new Random(DateTime.Now.Millisecond);
                if (rand.Next(1, 10) > 8 && nextQuestionId != 0)
                {
                    // Randomly give up, but never on the first question
                    return new GameResponse {Id = -1, Type = GameResponse.ResponseType.GiveUp};
                }
                var response = m_responses[nextQuestionId];
                Games[gameId] = ++nextQuestionId;
                return response;
            }
            catch (KeyNotFoundException e)
            {
                throw new KeyNotFoundException(string.Format("GameId not found: {0}", gameId), e);
            }
        }

        private static void EndGame(string gameId)
        {
            int value;
            Games.TryRemove(gameId, out value);
        }

    }
}