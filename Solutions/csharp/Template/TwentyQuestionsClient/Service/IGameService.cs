﻿using TwentyQuestionsClient.Models;

namespace TwentyQuestionsClient.Service
{
    public interface IGameService
    {
        GameNameResponse Name();
        GameResponse StartGame(string gameId);
        GameResponse HandleAnswer(string gameId, int questionId, GameResponse.Answer answer);
        void WinGame(string gameId, int questionId);
        void LoseGame(string gameId, int questionId);
    }
}
