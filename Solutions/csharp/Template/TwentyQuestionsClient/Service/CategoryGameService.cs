﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TwentyQuestionsClient.DTO;
using TwentyQuestionsClient.Models;
using TwentyQuestionsClient.Framework;



namespace TwentyQuestionsClient.Service
{
    public class CategoryGameService : IGameService
    {
        private static readonly ConcurrentDictionary<string, List<AnimalDTO>> Games =
            new ConcurrentDictionary<string, List<AnimalDTO>>();

        private static readonly ConcurrentDictionary<string, List<GameQuestion>> Questions =
            new ConcurrentDictionary<string, List<GameQuestion>>();


        private const int AnimalGuess = -1;
        private const int GiveUp = -2;
        private const int Comment = -3;

        public GameNameResponse Name()
        {
            return new GameNameResponse("Matthew's entry category-based game");
        }

        public Models.GameResponse StartGame(string gameId)
        {
            Games.AddOrUpdate(gameId, CreateListOfAnimals(), (s, list) => list);
            Questions.AddOrUpdate(gameId, new List<GameQuestion>(), (s, list) => list);
            return GenerateQuestion(gameId);
        }

        public Models.GameResponse HandleAnswer(string gameId, int questionId, Models.GameResponse.Answer answer)
        {
            if (questionId == AnimalGuess)
            {
                if (answer == GameResponse.Answer.Yes)
                {
                    return GenerateCommentResponse("#Winner!");
                }
                if (answer == GameResponse.Answer.No)
                {
                    StoreAnswerValue(gameId, questionId, answer);
                    var animals = Games[gameId];
                    animals.RemoveAt(0);
                }
                return GenerateQuestion(gameId);
            }

            if (questionId >= 0)
            {
                FilterRemainingAnimals(gameId, questionId, answer);
                return GenerateQuestion(gameId);
            }

            if (questionId == Comment)
            {
                return
                    GenerateCommentResponse(
                        "You shouldn't respond to a comment with yes or no. Can you click Win or Lose?");
            }

            throw new Exception("Unexpected response");

        }

        public void WinGame(string gameId, int questionId)
        {
            EndGame(gameId);
        }

        public void LoseGame(string gameId, int questionId)
        {
            EndGame(gameId);
        }

        /// <summary>
        /// Categorize the list of animals by the various enums that 
        /// are set up in AnimalDTO.cs
        /// </summary>
        /// <returns></returns>
        private static List<AnimalDTO> CreateListOfAnimals()
        {
            var dt = Data.Managers.Animals.GetAnimals();
            var dataRows = dt.Rows.Cast<DataRow>();
            var rows = dataRows as DataRow[] ?? dataRows.ToArray();

            var animalsList = rows.AsEnumerable().Select(row => new AnimalDTO
            {
                Name = Convert.ToString(row["name"]),
                Vertibrate = Convert.ToBoolean(row["vertibrate"]),
                Classification = (Classification?) row.GetNullableInt32("GroupId"),
                EatingClass = (EatingClass?) row.GetNullableInt32("FoodConsumptionId"),
                Habitat = (Habitat?) row.GetNullableInt32("HabitatId"),
                Continent = (Continent?) row.GetNullableInt32("ContinentId"),
                Mobility = (MobilityMethod?) row.GetNullableInt32("MobilityId"),
                Size = (Size?) row.GetNullableInt32("SizeId"),
                ReproductionMethod = (ReproductionMethod?) row.GetNullableInt32("ReproductionId"),
                Color = row.GetString("Color"),
                Characteristic1 = row.GetString("Characteristic1"),
                Characteristic2 = row.GetString("Characteristic2")
            }).ToList();

            return animalsList;
        }

        private static GameResponse GenerateQuestion(string gameId)
        {
            var animalsRemaining = Games[gameId];
            var questionList = Questions[gameId];

            return GenerateNextQuestion(questionList, animalsRemaining);
        }

        #region Functions for creating specific types of responses

        private static GameResponse GenerateNextQuestion(List<GameQuestion> questions, List<AnimalDTO> animals)
        {
            GameResponse response = null;
            response = animals.Count < 5
                ? GenerateAnimalGameResponse(animals)
                : GenerateCategoryGameResponse(questions, animals);
 
            return response ?? GenerateAnimalGameResponse(animals);
        }

        /// <summary>
        /// Look for a unique category among the animals. If we find one, we generate a question. Otherwise, we return null
        /// </summary>
        /// <param name="questions"></param>
        /// <param name="animals"></param>
        /// <returns></returns>
        private static GameResponse GenerateCategoryGameResponse(List<GameQuestion> questions,
            IReadOnlyList<AnimalDTO> animals)
        {
            GameResponse response = null;
            var maxItem = GetMaxItemFromCategories(questions,animals);
            if (maxItem != null)
                response = GetCategoryQuestion(questions, maxItem);
            return response;
        }

        /// Generate a question based on the first animal in the remaining list
        /// </summary>
        /// <param name="animalsRemaining"></param>
        /// <returns></returns>
        private static GameResponse GenerateAnimalGameResponse(List<AnimalDTO> animalsRemaining)
        {
            if (animalsRemaining.Any())
            {
                var animal = animalsRemaining.First();

                return new GameResponse
                {
                    Id = AnimalGuess,
                    Question = "Is it a/an " + animal.Name + "?",
                    Type = GameResponse.ResponseType.Question
                };
            }
            return GenerateGiveUpResponse();
        }

        private static GameResponse GenerateGiveUpResponse()
        {
            return new GameResponse
            {
                Id = GiveUp,
                Question = "I've run out of possible answers",
                Type = GameResponse.ResponseType.GiveUp
            };
        }

        private static GameResponse GenerateCommentResponse(string response)
        {
            return new GameResponse
            {
                Id = Comment,
                Question = response,
                Type = GameResponse.ResponseType.Question
            };
        }

        /// <summary>
        /// Gets the category that has the most items classified.   The idea is to narrow down the
        /// number of animals as quickly as possible.
        /// </summary>
        /// <param name="questions"></param>
        /// <param name="animals"></param>
        /// <returns></returns>
        private static MaxEnumItem GetMaxItemFromCategories(List<GameQuestion> questions, IReadOnlyList<AnimalDTO> animals)

        {
            List<MaxEnumItem> maxList = new List<MaxEnumItem>();

            var totalCount = animals.Count;
            var vertibrateCount = animals.AsEnumerable().Count(x => x.Vertibrate);
            if (!HaveAnswer(questions, "Classification"))
                AddCategoryItemToList(maxList, animals, item => item.Classification, typeof (Classification).Name);
            if (!HaveAnswer(questions, "Habitat"))
                AddCategoryItemToList(maxList, animals, item => item.Habitat, typeof (Habitat).Name);
            if (!HaveAnswer(questions, "MobilityMethod"))
                AddCategoryItemToList(maxList, animals, item => item.Mobility, typeof (MobilityMethod).Name);
            if (!HaveAnswer(questions, "EatingClass"))
                AddCategoryItemToList(maxList, animals, item => item.EatingClass, typeof (EatingClass).Name);
            if (!HaveAnswer(questions, "ReproductionMethod"))
                AddCategoryItemToList(maxList, animals, item => item.ReproductionMethod,
                    typeof (ReproductionMethod).Name);
            if (!HaveAnswer(questions, "Size"))
                AddCategoryItemToList(maxList, animals, item => item.Size, typeof (Size).Name);
            if (!HaveAnswer(questions, "Continent"))
                AddCategoryItemToList(maxList, animals, item => item.Continent, typeof (Continent).Name);
            return maxList.OrderByDescending(x => x.Count).FirstOrDefault();
        }

        private static bool HaveAnswer(List<GameQuestion> questions, string enumName)
        {
            return
                questions.Any(
                    x =>
                        x.EnumName == enumName
                        && (x.Answer == GameResponse.Answer.Yes || x.Answer == GameResponse.Answer.Skip));
        }

        public static void AddCategoryItemToList<TKey>(List<MaxEnumItem> maxList, IReadOnlyList<AnimalDTO> animals,
            Func<AnimalDTO, TKey> groupingProperty, string enumClass)
        {
            var x = animals.GroupBy(groupingProperty).Where(items =>items.Key.ToString() != string.Empty) .Select(a =>
                new MaxEnumItem
                {
                    Key = a.Key.ToString(),
                    Count = a.Count(),
                    EnumName = enumClass
                }
                ).OrderByDescending(g => g.Count);
            var maxItem =  x.FirstOrDefault();
            if(maxItem != null) maxList.Add(maxItem);
        }

        /// <summary>
        /// Generate the questions by the category from the 
        /// category that has the most items in the animals list
        /// </summary>
        /// <param name="questions"></param>
        /// <param name="enumItem"></param>
        /// <returns></returns>
        private static GameResponse GetCategoryQuestion(List<GameQuestion> questions, MaxEnumItem enumItem)
        {
            var questionItem = new GameQuestion();

            switch (enumItem.EnumName)
            {
                case "Classification":
                    questionItem = GetClassificationQuestion(enumItem);
                    break;
                case "Habitat":
                    questionItem = GetHabitatQuestion(enumItem);
                    break;
                case "MobilityMethod":
                    questionItem = GetMobilityQuestion(enumItem);
                    break;
                case "EatingClass":
                    questionItem = GetEatingQuestion(enumItem);
                    break;
                case "ReproductionMethod":
                    questionItem = GetReproductionQuestion(enumItem);
                    break;
                case "Size":
                    questionItem = GetSizeQuestion(enumItem);
                    break;
                case "Continent":
                    questionItem = GetContinentQuestion(enumItem);
                    break;

            }
            //
            questions.Add(questionItem);

            var response = new GameResponse()
            {
                Id = questionItem.Id,
                Question = questionItem.Question,
                Type = GameResponse.ResponseType.Question

            };

            return response;
        }

        private static GameQuestion GetClassificationQuestion(MaxEnumItem enumItem)
        {
            var x = ParseEnum<Classification>(enumItem.Key, true);
            var questionItem = new GameQuestion();
            questionItem.EnumName = typeof(Classification).Name;
            questionItem.EnumValue = (int)(Classification)x;
            switch ((Classification) x)
            {
                case Classification.Mammal:
                    questionItem.Id = 100;
                    questionItem.Question = "Is your animal a mammal?";
                    break;
                case Classification.Bird:
                    questionItem.Id = 101;
                    questionItem.Question = "Is your animal a bird?";
                    break;
                case Classification.Fish:
                    questionItem.Id = 102;
                    questionItem.Question = "Is your animal a fish?";
                    break;

                case Classification.Amphibian:
                    questionItem.Id = 103;
                    questionItem.Question = "Is your animal an amphibian?";
                    break;
                case Classification.Reptile:
                    questionItem.Id = 104;
                    questionItem.Question = "Is your animal a reptile?";
                    break;
                case Classification.Insect:
                    questionItem.Id = 105;
                    questionItem.Question = "Is your animal an insect?";
                    break;
                case Classification.Arthropod:
                    questionItem.Id = 106;
                    questionItem.Question = "Does your animal have six or more legs?";
                    break;
                case Classification.Cephalopod:
                    questionItem.Id = 107;
                    questionItem.Question = "Is your animal a squid or octopus?";
                    break;
                case Classification.Mollusk:
                    questionItem.Id = 108;
                    questionItem.Question = "Does your animal have a shell?";
                    break;
                case Classification.Echinoderms:
                    questionItem.Id = 109;
                    questionItem.Question = "Is your animal a type of weird sea creature with lots of arms?";
                    break;
            }
            return questionItem;
        }
        private static GameQuestion GetHabitatQuestion(MaxEnumItem enumItem)
        {
            var x = ParseEnum<Habitat>(enumItem.Key, true);
            var questionItem = new GameQuestion();
            questionItem.EnumName = typeof(Habitat).Name;
            questionItem.EnumValue = (int)(Habitat)x;
            switch ((Habitat) x)
            {
                case Habitat.Ground:
                    questionItem.Id = 200;
                    questionItem.Question = "Does this animal live on the ground?";
                    break;
                case Habitat.Trees:
                    questionItem.Id = 201;
                    questionItem.Question = "Does this animal live in the trees?";
                    break;
                case Habitat.Water:
                    questionItem.Id = 202;
                    questionItem.Question = "Does this animal live in the water?";
                    break;
                case Habitat.Marsh:
                    questionItem.Id = 203;
                    questionItem.Question = "Does this animal live in a marsh or wetland?";
                    break;
                case Habitat.UnderGround:
                    questionItem.Id = 204;
                    questionItem.Question = "Does this animal live underground?";
                    break;
                case Habitat.Cave:
                    questionItem.Id = 205;
                    questionItem.Question = "Does this animal live in cave?";
                    break;
                case Habitat.Desert:
                    questionItem.Id = 206;
                    questionItem.Question = "Does this animal live in the desert?";
                    break;
                case Habitat.Seabird:
                    questionItem.Id = 207;
                    questionItem.Question = "Is this animal a seabird?";
                    break;

            }
            return questionItem;
        }
        private static GameQuestion GetMobilityQuestion(MaxEnumItem enumItem)
        {
            var x = ParseEnum<MobilityMethod>(enumItem.Key, true);
            var questionItem = new GameQuestion();
            questionItem.EnumName = typeof(MobilityMethod).Name;
            questionItem.EnumValue = (int)(MobilityMethod)x;
            switch ((MobilityMethod)x)
            {
                case MobilityMethod.Climb:
                    questionItem.Id = 300;
                    questionItem.Question = "Is your animal able to climb?";
                    break;
                case MobilityMethod.Fly:
                    questionItem.Id = 301;
                    questionItem.Question = "Is your animal able to fly?";
                    break;
                case MobilityMethod.Hop:
                    questionItem.Id = 302;
                    questionItem.Question = "Is your animal able to hop?";
                    break;
                case MobilityMethod.Run:
                    questionItem.Id = 303;
                    questionItem.Question = "Is your animal able to run?";
                    break;
                case MobilityMethod.Slither:
                    questionItem.Id = 304;
                    questionItem.Question = "Does your animal crawl on the ground?";
                    break;
                case MobilityMethod.Swim:
                    questionItem.Id = 305;
                    questionItem.Question = "Does your animal swim?";
                    break;
                case MobilityMethod.Walk:
                    questionItem.Id = 306;
                    questionItem.Question = "Does your animal get around by walking?";
                    break;
            }
            return questionItem;
        }
        private static GameQuestion GetEatingQuestion(MaxEnumItem enumItem)
        {
            var x = ParseEnum<EatingClass>(enumItem.Key, true);
            var questionItem = new GameQuestion();
            questionItem.EnumName = typeof(EatingClass).Name;
            questionItem.EnumValue = (int)(EatingClass)x;
            switch ((EatingClass) x)
            {
                case EatingClass.Carnivore:
                    questionItem.Id = 400;
                    questionItem.Question = "Does your animal eat meat?";
                    break;
                case EatingClass.Herbivore:
                    questionItem.Id = 401;
                    questionItem.Question = "Does your animal eat plants and vegetables?";
                    break;
                case EatingClass.Ominvore:
                    questionItem.Id = 402;
                    questionItem.Question = "Can your animal eat either meat or plants and vegetables?";
                    break;
            }
            return questionItem;
        }
        private static GameQuestion GetReproductionQuestion(MaxEnumItem enumItem)
        {
            var x = ParseEnum<ReproductionMethod>(enumItem.Key, true);
            var questionItem = new GameQuestion();
            questionItem.EnumName = typeof(ReproductionMethod).Name;
            questionItem.EnumValue = (int)(ReproductionMethod)x;
            switch ((ReproductionMethod)x)
            {
                case ReproductionMethod.InternalDevelopment:
                    questionItem.Id = 500;
                    questionItem.Question = "Does your animal's babies develop internally?";
                    break;
                case ReproductionMethod.EggLayer:
                    questionItem.Id = 500;
                    questionItem.Question = "Does your animal lay eggs?";
                    break;
            }
            return questionItem;
        }
        private static GameQuestion GetSizeQuestion(MaxEnumItem enumItem)
        {
            var x = ParseEnum<Size>(enumItem.Key, true);
            var questionItem = new GameQuestion();
            questionItem.EnumName = typeof(Size).Name;
            questionItem.EnumValue = (int)(Size)x;
            switch ((Size)x)
            {
                case Size.VerySmall:
                    questionItem.Id = 600;
                    questionItem.Question = "Is your smaller than a mouse?";
                    break;
                case Size.Small:
                    questionItem.Id = 601;
                    questionItem.Question = "Is your about the same size as a mouse?";
                    break;
                case Size.Medium:
                    questionItem.Id = 602;
                    questionItem.Question = "Is your about the same size as a cat?";
                    break;
                case Size.Large:
                    questionItem.Id = 603;
                    questionItem.Question = "Is your about the same size as a horse?";
                    break;
                case Size.VeryLarge:
                    questionItem.Id = 604;
                    questionItem.Question = "Is your larger than a horse?";
                    break;
            }
            return questionItem;
        }
        private static GameQuestion GetContinentQuestion(MaxEnumItem enumItem)
        {
            var x = ParseEnum<Continent>(enumItem.Key, true);
            var questionItem = new GameQuestion();
            questionItem.EnumName = typeof(Continent).Name;
            questionItem.EnumValue = (int)(Continent)x;
            switch ((Continent)x)
            {
                case Continent.Africa:
                    questionItem.Id = 700;
                    questionItem.Question = "Does your animal live primarily in Africa?";
                    break;
                case Continent.Antartica:
                    questionItem.Id = 701;
                    questionItem.Question = "Does your animal live primarily in Antartica?";
                    break;
                case Continent.Asia:
                    questionItem.Id = 702;
                    questionItem.Question = "Does your animal live primarily in Asia?";
                    break;
                case Continent.Australia:
                    questionItem.Id = 703;
                    questionItem.Question = "Does your animal live primarily in Australia?";
                    break;
                case Continent.Europe:
                    questionItem.Id = 704;
                    questionItem.Question = "Does your animal live primarily in Europe?";
                    break;
                case Continent.NorthAmerica:
                    questionItem.Id = 705;
                    questionItem.Question = "Does your animal live primarily in North America?";
                    break;
                case Continent.SouthAmerica:
                    questionItem.Id = 706;
                    questionItem.Question = "Does your animal live primarily in South America?";
                    break;
                case Continent.Worldwide:
                    questionItem.Id = 707;
                    questionItem.Question = "Does your animal live all over the world?";
                    break;
            }
            return questionItem;

        }
        #endregion

        #region HelperMethod

        private static void EndGame(string gameId)
        {
            List<AnimalDTO> value;
            Games.TryRemove(gameId, out value);
        }

        private static TEnum ParseEnum<TEnum>(string item, bool ignorecase = default(bool))
            where TEnum : struct
        {
            TEnum tenumResult = default(TEnum);
            return Enum.TryParse<TEnum>(item, ignorecase, out tenumResult)
                ? tenumResult
                : default(TEnum);
        }
        #endregion

        #region FilterAnimals Methods
        private static void FilterRemainingAnimals(string gameId, int questionId, GameResponse.Answer answer)
        {
            StoreAnswerValue(gameId, questionId, answer);

            if (answer != GameResponse.Answer.Yes && answer != GameResponse.Answer.No) return;

            var animals = Games[gameId];
            FilterAnimalsByCategory(gameId, animals, questionId, answer);
            Games[gameId] = animals;
        }

        private static void setLearningMode(string gameId)
        {
            var animals = CreateListOfAnimals();
             Games[gameId] = animals ;
            var questionList = Questions[gameId];
            
            // re-establish the classification
            var questionId =
                questionList.AsEnumerable()
                    .First(item => item.EnumName == "Classification" && item.Answer == GameResponse.Answer.Yes)
                    .Id;
            FilterRemainingAnimals(gameId, questionId, GameResponse.Answer.Yes);

            animals = Games[gameId];
            var yesQuestions =
                questionList.AsEnumerable()
                    .Where(item => item.EnumName != "Classification" && item.Answer == GameResponse.Answer.Yes)
                    .ToList();
            foreach (var gameQuestion in yesQuestions)
            {
                FilterAnimalsByCategory(gameId, animals, gameQuestion.Id, GameResponse.Answer.Yes, true);
            }
            Games[gameId] = animals;

        }
        private static void FilterAnimalsByCategory(string gameId, List<AnimalDTO> animals, int questionId,
            GameResponse.Answer answer, bool includeNulls = false)
        {
            var questions = Questions[gameId];

            Predicate<AnimalDTO> pred = null;

            var qItem = questions.FirstOrDefault(x => x.Id == questionId);
            if (qItem == null) return;

            switch (qItem.EnumName)
            {
                case "Classification":
                    pred = answer == GameResponse.Answer.Yes
                        ? (Predicate<AnimalDTO>) (item => item.Classification != (Classification?) qItem.EnumValue)
                        : (item => item.Classification == (Classification?) qItem.EnumValue);

                    break;

                case "Habitat":
                    pred = answer == GameResponse.Answer.Yes
                        ? (Predicate<AnimalDTO>) (item => item.Habitat != (Habitat?) qItem.EnumValue && !includeNulls ||
                            item.Habitat != (Habitat?)qItem.EnumValue && item.Habitat != null)
                        : (item => item.Habitat == (Habitat?) qItem.EnumValue);
                    break;
                case "EatingClass":
                    pred = answer == GameResponse.Answer.Yes
                        ? (Predicate<AnimalDTO>) (item => item.EatingClass != (EatingClass?) qItem.EnumValue)
                        : (item => item.EatingClass == (EatingClass?) qItem.EnumValue);
                    break;

                case "MobilityMethod":
                    pred = answer == GameResponse.Answer.Yes
                        ? (Predicate<AnimalDTO>)(item => item.Mobility != (MobilityMethod?)qItem.EnumValue && !includeNulls ||
                            item.Mobility != (MobilityMethod?)qItem.EnumValue && item.Mobility != null)
                        : (item => item.Mobility == (MobilityMethod?) qItem.EnumValue);
                    break;

                case "Size":
                    pred = answer == GameResponse.Answer.Yes
                        ? (Predicate<AnimalDTO>) (item => item.Size != (Size?) qItem.EnumValue)
                        : (item => item.Size == (Size?) qItem.EnumValue);
                    break;

                case "Continent":
                    pred = answer == GameResponse.Answer.Yes
                        ? (Predicate<AnimalDTO>) (item => item.Continent != (Continent?) qItem.EnumValue)
                        : (item => item.Continent == (Continent?) qItem.EnumValue);
                    break;
                case "Color":
                    pred = answer == GameResponse.Answer.Yes
                        ? (Predicate<AnimalDTO>) (item => item.Color != qItem.Characteristic)
                        : (item => item.Color == qItem.Characteristic);
                    break;
                case "Characteristic1":
                    pred = answer == GameResponse.Answer.Yes
                        ? (Predicate<AnimalDTO>) (item => item.Characteristic1 != qItem.Characteristic)
                        : (item => item.Characteristic1 == qItem.Characteristic);
                    break;
                case "Characteristic2":
                    pred = answer == GameResponse.Answer.Yes
                        ? (Predicate<AnimalDTO>) (item => item.Characteristic2 != qItem.Characteristic)
                        : (item => item.Characteristic2 == qItem.Characteristic);
                    break;

            }
            if (pred != null) FilterAnimals(animals, pred);
        }

        private static void FilterAnimals(List<AnimalDTO> animals,
            Predicate<AnimalDTO> groupingProperty)
        {
            animals.RemoveAll(groupingProperty);
        }

        private static void StoreAnswerValue(string gameId, int questionId, GameResponse.Answer answer)
        {
            var questions = Questions[gameId];
            var questionItem = questions.AsEnumerable().FirstOrDefault(x => x.Id == questionId);
            if (questionItem != null)
            {
                questionItem.Answer = answer;
            }
        }

        #endregion


    }
}