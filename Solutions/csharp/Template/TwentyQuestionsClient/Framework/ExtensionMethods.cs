﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace TwentyQuestionsClient.Framework
{
    public static class ExtensionMethods
    {
        public static int? GetNullableInt32(this DataRow row, string columnName)
        {
            int? value = null;
            if (!row.IsNull(columnName))
                value = Convert.ToInt32(row[columnName]);

            return value;
        }

        public static string GetString(this DataRow row, string columnName)
        {
            return Convert.ToString(row[columnName]);
        }

        public static DateTime GetDateTime(this DataRow row, string columnName)
        {
            return GetNullableDateTime(row, columnName).GetValueOrDefault();
        }
        public static DateTime? GetNullableDateTime(this DataRow row, string columnName)
        {
            DateTime? value = null;
            if (!row.IsNull(columnName))
                value = Convert.ToDateTime(row[columnName]);

            return value;
        }

    }
}