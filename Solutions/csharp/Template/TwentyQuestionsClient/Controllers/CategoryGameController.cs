﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TwentyQuestionsClient.Models;
using TwentyQuestionsClient.Service;

namespace TwentyQuestionsClient.Controllers
{
    public class CategoryGameController : ApiController
    {
        private readonly IGameService m_gameService = new CategoryGameService();

        // GET api/values
        [HttpGet]
        [Route("category/name")]
        public GameNameResponse Get()
        {
            return m_gameService.Name();
        }

        [HttpPost]
        [Route("category/game/{gameid}/start")]
        [ResponseType(typeof(GameResponse))]
        public IHttpActionResult PostStart(string gameid)
        {
            return Ok(m_gameService.StartGame(gameid));
        }
                [HttpPost]
        [Route("category/game/{gameid}/answer/{questionid}/{response}")]
        [ResponseType(typeof(GameResponse))]
        public IHttpActionResult PostAnswer(string gameid, int questionid, string response)
        {
            switch (response)
            {
                case "yes":
                    return Ok(m_gameService.HandleAnswer(gameid, questionid, GameResponse.Answer.Yes));
                case "no":
                    return Ok(m_gameService.HandleAnswer(gameid, questionid, GameResponse.Answer.No));
                case "skip":
                    return Ok(m_gameService.HandleAnswer(gameid, questionid, GameResponse.Answer.Skip));
                case "win":
                    m_gameService.WinGame(gameid, questionid);
                    return Ok();
                case "lose":
                    m_gameService.LoseGame(gameid, questionid);
                    return Ok();
                default:
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}
