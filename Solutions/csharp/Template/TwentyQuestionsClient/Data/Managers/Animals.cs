﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace TwentyQuestionsClient.Data.Managers
{
    public class Animals
    {
       public static DataTable GetAnimals()
        {
            var conn = new SqlConnection(GetConnectionString());
            try
            {
                conn.Open();
                var cmd = new SqlCommand("select * from Animals", conn) {CommandType = CommandType.Text};
                var table = new DataTable();
                var adapt = new SqlDataAdapter {SelectCommand = cmd};
                adapt.Fill(table);
                return table;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

       private static string GetConnectionString()
       {
           return String.Format("{0}{1}{2}", @"Data Source=(LocalDB)\v11.0;AttachDbFilename=", HttpRuntime.AppDomainAppPath, @"App_Data\TwentyQuestions.mdf;Integrated Security=True;Connect Timeout=30");
       }

       #region Data setup
       public void UpdateSQLFromXLS()
        {
            var dt = extractXlsToDataTable();
            //var cmd = new SqlCommand();
            //cmd.CommandType = CommandType.Text;

            //SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            //var dataSet = new DataSet();
            //adapter.Fill(dt);
            //new SqlCommandBuilder(adapter);
            //adapter.Update();
            var conn =
                new SqlConnection(GetConnectionString());
            try
            {
                conn.Open();
                const string sql = "INSERT INTO animals(Id,Name, vertibrate,GroupId) VALUES(@param1,@param2,@param3,@param4)";
                var i = 4;
                foreach (DataRow row in dt.Rows)
                {
                    var value = row[0].ToString().Trim();
                    var cmd = new SqlCommand(sql, conn) { CommandType = CommandType.Text };
                    cmd.Parameters.Add("@param1", System.Data.SqlDbType.Int).Value = i++;
                    cmd.Parameters.Add("@param2", System.Data.SqlDbType.NVarChar, 50).Value = value;
                    cmd.Parameters.Add("@param3", System.Data.SqlDbType.Bit, 50).Value = true;
                    cmd.Parameters.Add("@param4", System.Data.SqlDbType.Int, 50).Value = 1;

                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private DataTable extractXlsToDataTable()
        {
            const string path = @"D:\projects\Template\TwentyQuestionsClient\App_Data\animals.xls";
            const string sheetName = "Sheet1";
            using (var conn = new OleDbConnection())
            {
                var dt = new DataTable();
                var importFileName = path;
                var fileExtension = Path.GetExtension(importFileName);
                if (fileExtension == ".xls")
                    conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + importFileName + ";" +
                                            "Extended Properties='Excel 8.0;HDR=YES;'";
                if (fileExtension == ".xlsx")
                    conn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + importFileName + ";" +
                                            "Extended Properties='Excel 12.0 Xml;HDR=YES;'";
                using (var comm = new OleDbCommand())
                {
                    comm.CommandText = "Select * from [" + sheetName + "$]";

                    comm.Connection = conn;

                    using (OleDbDataAdapter da = new OleDbDataAdapter())
                    {
                        da.SelectCommand = comm;
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
        }
       #endregion

    }
}