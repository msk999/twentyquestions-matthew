﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwentyQuestionsClient.DTO
{
    public enum Classification
    {
        Unknown,
        Mammal,
        Bird,
        Amphibian,
        Fish,
        Insect,  //Arthropod,  Insects, scorpions
        Mollusk, // snails, oysters,
        Worms,
        Cephalopod,// Octobpus, squids
        Arthropod, // crustaceans, spiders, centipedes
        Echinoderms, // starfish sea urchins
        Reptile

        };

    public enum Backbone
    {
        Vertibrate,
        Invertibrate
    }

    public enum EatingClass
    {
        Unknown,
        Carnivore,
        Ominvore,
        Herbivore
    };

    public enum ReproductionMethod
    {
        Unknown,
        InternalDevelopment,
        EggLayer
    }

    public enum MobilityMethod
    {
        Unknown,
        Fly,
        Swim,
        Climb,
        Run,
        Hop,
        Slither,
        Walk
    }

    public enum Size
    {
        Unknown,
        VerySmall,
        Small,
        Medium,
        Large,
        VeryLarge
    }

    public enum Habitat
    {
        Unknown,
        Ground,
        Water,
        Trees,
        Marsh,
        UnderGround,
        Cave,
        Seabird,
        Desert
    }

    public enum Continent
    {
        Unknown,
        Asia,
        Africa,
        Antartica,
        Australia,
        Europe,
        NorthAmerica,
        SouthAmerica,
        Worldwide
    }

    public class AnimalDTO
    {
        public string Name { get; set; }
        public bool Vertibrate { get; set; }
        public Classification? Classification { get; set; }
        public Habitat? Habitat { get; set; }
        public ReproductionMethod? ReproductionMethod { get; set; }
        public EatingClass? EatingClass { get; set; }
        public MobilityMethod? Mobility { get; set; }
        public Size? Size { get; set; }
        public Continent? Continent { get; set; }
        public String Color { get; set; }
        public string Characteristic1 { get; set; }
        public string Characteristic2 { get; set; }
    }

    public class MaxEnumItem
    {
        public string Key { get; set; }
        public decimal Count { get; set; }
        public string EnumName { get; set; }
    }


}