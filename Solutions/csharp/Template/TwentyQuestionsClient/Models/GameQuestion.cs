﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwentyQuestionsClient.Models
{
    public enum CategoryId
    {
        Classification = 100,
        Habitat = 200,
        Mobility = 300
    }
    
    public class GameQuestion
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string EnumName { get; set; }
        public int EnumValue { get; set; }
        public string Characteristic { get; set; }
        public GameResponse.Answer Answer { get; set; }
    }
}